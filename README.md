#Maturity Calculator

A simple application to value maturing insurance policies. Utilises a simple class utility to aid the calculation.

##Usage

    ./calc_maturity.rb [CSV file path] [Desired XML filename (incl extension)]
    
#TODO

- Implement XML file output;
- Fix XML duplication in output;
