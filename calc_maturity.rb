#!/usr/bin/env ruby
require 'builder'
require 'csv'
require './policy.rb'

csv_file = ARGV[0]
xml_file = ARGV[1]
policies = ''
xml = Builder::XmlMarkup.new(:indent => 2)
xml.instruct! :xml, :encoding => "UTF-8"

CSV.foreach(csv_file, {:headers => true, :header_converters => :symbol}) do |row|
    policy = Policy.new(row[:policy_number],row[:policy_start_date],row[:premiums],row[:membership],row[:discretionary_bonus],row[:uplift_percentage])
    policies << xml.policy { |p| p.number = policy.number; p.value = policy.value }
end

puts policies