require 'date'

class Policy
    @@base_date = Date.new(1990, 01, 01)
    
    attr_reader :number
    
    def initialize policy_number, policy_start_date, premiums, membership, discretionary_bonus, uplift_percentage
        @number = policy_number
        @start = Date.strptime(policy_start_date, "%d/%m/%Y")
        @premiums = premiums.to_i
        @membership = membership
        @bonus = discretionary_bonus.to_i if self.bonus? || 0
        @uplift = uplift_percentage.to_i
        @type = self.type
    end
    
    def type
        allowed = ['A', 'B', 'C']
        if allowed.include? @number[0]
            @number[0]
        else
            raise 'Invalid policy number.'
        end
    end
    
    def bonus?
        if @type == 'A'
            @@base_date > @start
        elsif @type == 'B'
            @membership == 'Y'
        elsif @type == 'C'
            @@base_date <= @start && @membership == 'Y'
        end
    end
    
    def fee
        fees = {'A' => 3, 'B' => 5, 'C' => 7}
        fees[@type]
    end
    
    def value
        management_fee = @premiums * self.fee / 100
        uplift = (@uplift / 100) + 1
        value = (((@premiums - management_fee) + @bonus) * uplift)
    end
end